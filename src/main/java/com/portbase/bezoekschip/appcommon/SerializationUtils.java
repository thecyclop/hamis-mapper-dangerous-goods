package com.portbase.bezoekschip.appcommon;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import com.portbase.bezoekschip.common.utils.FileUtils;
import io.fluxcapacitor.javaclient.common.serialization.jackson.JacksonSerializer;
import lombok.SneakyThrows;

import java.util.Arrays;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY;
import static com.fasterxml.jackson.databind.ObjectMapper.DefaultTyping.JAVA_LANG_OBJECT;

public class SerializationUtils {
    private static final JsonMapper jsonMapper = JacksonSerializer.defaultObjectMapper.rebuild()
            .activateDefaultTyping(LaissezFaireSubTypeValidator.instance, JAVA_LANG_OBJECT, PROPERTY)
            .build();

    @SneakyThrows
    public static Object deserialize(String fileName) {
        return jsonMapper.readValue(FileUtils.loadFile(fileName), Object.class);
    }

    @SuppressWarnings("unchecked")
    @SneakyThrows
    public static <T> T deserializeRaw(String json) {
        return (T) deserializeRaw(json, Object.class);
    }

    @SneakyThrows
    public static <T> T deserializeRaw(String json, Class<T> type) {
        return jsonMapper.readValue(json, type);
    }

    @SneakyThrows
    public static <T> T deserializeRaw(byte[] json, Class<T> type) {
        return jsonMapper.readValue(json, type);
    }

    @SneakyThrows
    public static List<?> deserializeList(String fileName) {
        return jsonMapper.readValue(FileUtils.loadFile(fileName), List.class);
    }

    public static Object[] deserialize(String... fileNames) {
        return Arrays.stream(fileNames).map(SerializationUtils::deserialize).toArray();
    }
}
