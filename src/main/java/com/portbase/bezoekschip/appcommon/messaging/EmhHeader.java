package com.portbase.bezoekschip.appcommon.messaging;

import lombok.Data;

@Data
public class EmhHeader {
    String msgType;
    String msgFunction;
    String techMsgFunction;
    String senderOrgShortName;
    String recipientOrgShortName;
    String msgReference;
    String previousMsgReference;
    BusinessDataCorrelation  businessDataCorrelation;

    @Data
    public static class BusinessDataCorrelation {
        String businessDataCorrelationId;
    }
}