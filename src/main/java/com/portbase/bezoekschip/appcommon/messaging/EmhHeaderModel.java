package com.portbase.bezoekschip.appcommon.messaging;

import lombok.Builder;
import lombok.Value;

import static com.portbase.bezoekschip.appcommon.messaging.MessengerUtils.createMessageWithoutChangingContent;


@Value
@Builder
public class EmhHeaderModel {
    private static final String TEMPLATE_FILE = "emh-header.mustache";

    String payload;
    String goal;
    String receiver;
    String type;

    //optional
    String responseQueue;
    String processId;
    String crn;
    String smallType;
    String previousMessageReference;
    boolean singleWindow;

    public String processId(){
        return processId ==null? crn + "_" + smallType : processId;
    }

    public String responseQueue(){
        return responseQueue == null? null : "\n                    <ns2:responseQueueJndiName>" + responseQueue + "</ns2:responseQueueJndiName>";
    }

    public String previousMessageReference(){
        return previousMessageReference == null? null : "\n                <ns2:previousMsgReference>" + previousMessageReference + "</ns2:previousMsgReference>";
    }

    public String messageReferenceType(){
        return singleWindow ? "\n                <ns3:messageReferenceType>SingleWindowReference</ns3:messageReferenceType>": null;
    }

    public String toString() {
        return createMessageWithoutChangingContent(TEMPLATE_FILE, this);
    }
}
