package com.portbase.bezoekschip.appcommon.messaging;

import com.samskivert.mustache.Escapers;
import com.samskivert.mustache.Mustache;
import lombok.extern.slf4j.Slf4j;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import static com.portbase.bezoekschip.appcommon.XmlUtils.*;

@Slf4j
public class MessengerUtils {


    public static final String emhJmsAddress = "jms/portbase/topic/us/emh/request/2";
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ssXXX");
    private static final String uopid = String.valueOf(UUID.randomUUID().getMostSignificantBits());

    public static String createMessage(String templateFile, Object model) {
        return createXmlMessage(computeTemplate(templateFile, MessengerUtils::compiler, MessengerUtils.class), model);
    }

    public static String createMessageWithoutChangingContent(String templateFile, Object model) {
        return computeTemplate(templateFile, () -> compiler().withEscaper(Escapers.NONE), MessengerUtils.class).execute(model);
    }

    private static Mustache.Compiler compiler() {
        return XML_COMPILER.withFormatter(value -> {
            if (value instanceof Instant) {
                return dateTimeFormatter.format(((Instant) value).atZone(ZoneId.of("Europe/Amsterdam")));
            }
            return value.toString();
        }).defaultValue("");
    }

}
