package com.portbase.bezoekschip.appcommon;

import com.samskivert.mustache.Escapers;
import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.Template;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import javax.xml.bind.JAXBContext;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.util.StreamReaderDelegate;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.portbase.bezoekschip.common.utils.FileUtils.loadFile;
import static java.util.regex.Pattern.compile;

@Slf4j
public class XmlUtils {
    private static final Map<String, Template> templates = new ConcurrentHashMap<>();

    public static final Mustache.Compiler XML_COMPILER = Mustache.compiler().withEscaper(
            Escapers.simple(new String[]{"&", "&amp;"}, new String[]{"'", "&apos;"}, new String[]{"\"", "&quot;"},
                    new String[]{"<", "&lt;"}, new String[]{">", "&gt;"}, new String[] {"\n", " "}));
    private static final Pattern emptyTagsPattern = compile("\\s*<[\\w\\s:\"=]+></[\\w\\s:\"=]+>|\\s*<[\\w\\s:\"=]+>\n*\\s*</[\\w\\s:\"=]+>");
    private static final Pattern asciiCharacters = compile("[^\\u0009\\u000a\\u000d\\u0020-\\uD7FF\\uE000-\\uFFFD]");
    public static final String DO_NOT_REMOVE_ATTRIBUTE = "#doNotRemoveTag";
    private static final Pattern doNotRemovePattern = compile("\\s*" + DO_NOT_REMOVE_ATTRIBUTE + "\\s*");

    public static Template computeTemplate(String fileName, Supplier<Mustache.Compiler> compiler) {
        return templates.computeIfAbsent(fileName, f -> compiler.get().compile(loadFile(fileName)));
    }

    public static Template computeTemplate(String fileName, Supplier<Mustache.Compiler> compiler, Charset charset) {
        return templates.computeIfAbsent(fileName, f -> compiler.get().compile(loadFile(fileName, charset)));
    }

    public static Template computeTemplate(String fileName, Supplier<Mustache.Compiler> compiler, Class<?> referencePoint) {
        return templates.computeIfAbsent(fileName, f -> compiler.get().compile(loadFile(referencePoint, fileName)));
    }

    public static String createXmlMessage(Template template, Object model) {
        return createXmlMessage(template, model, true);
    }

    public static String createXmlMessage(Template template, Object model, boolean removeEmptyTags) {
        String message = template.execute(model);
        if (removeEmptyTags) {
            String previous = null;
            while (!message.equals(previous)) {
                previous = message;
                message = emptyTagsPattern.matcher(message).replaceAll("");
            }
            message = doNotRemovePattern.matcher(message).replaceAll("");
        }
        message = asciiCharacters.matcher(message).replaceAll("");
        return message;
    }

    @SneakyThrows
    public static <T> T deserialize(String xml, Class<T> type) {
        XMLStreamReader xsr = new XsiTypeReader(XMLInputFactory.newFactory().createXMLStreamReader(new StringReader(xml)));
        return JAXBContext.newInstance(type).createUnmarshaller().unmarshal(xsr, type).getValue();
    }

    // used to deactivate namespaces
    private static class XsiTypeReader extends StreamReaderDelegate {
        XsiTypeReader(XMLStreamReader reader) {
            super(reader);
        }

        @Override
        public String getNamespaceURI() {
            return "";
        }
    }

    public static String xmlTagsToCamelCaseOrLowerCase(String input) {

        //all uppercase will become all lowercase
        Matcher m = compile("</?[A-Z_0-9]+:[A-Z_0-9]+[\\s>]").matcher(input);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            String[] split = m.group().split(":");
            m.appendReplacement(sb, split[0] + ":" + split[1].toLowerCase());
        }
        m.appendTail(sb);

        // uppercase camelcase will become lowercase camelcase
        m = Pattern.compile("</?[A-Z_0-9]+:[A-Z_]").matcher(sb.toString());
        sb = new StringBuffer();
        while (m.find()) {
            String[] split = m.group().split(":");
            m.appendReplacement(sb, split[0] + ":" + split[1].toLowerCase());
        }
        m.appendTail(sb);
        return sb.toString();
    }

    public static String removeInvalidXmlCharactersFromXml(String input) {

        //remove &
        Matcher m = compile("&[^a]").matcher(input);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(sb, "&amp;"+ m.group().substring(1));
        }
        m.appendTail(sb);

        // remove >
        m = compile("<[^<>]*>[^<>]*>").matcher(sb.toString());
        sb = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(sb, m.group().substring(0, m.group().length() - 1) + "&gt;");
        }
        m.appendTail(sb);

        // remove <
        m = compile("<[^<>]*</[^<>]*>").matcher(sb.toString());
        sb = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(sb, "&lt;" + m.group().substring(1));
        }
        m.appendTail(sb);

        //remove '
        m = compile("'[^>]*<").matcher(sb.toString());
        sb = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(sb, "&apos;" + m.group().substring(1));
        }
        m.appendTail(sb);

        //remove "
        m = compile("\"[^>]*<").matcher(sb.toString());
        sb = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(sb, "&quot;" + m.group().substring(1));
        }
        m.appendTail(sb);


        return sb.toString();
    }
}
