package com.portbase.bezoekschip.appcommon;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class MapperUtils {
    public static DecimalFormat decimalFormatter(int minimumFractionDigits, int maximumFractionDigits) {
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator('.');
        DecimalFormat decimalFormatter = new DecimalFormat();
        decimalFormatter.setMinimumFractionDigits(minimumFractionDigits);
        decimalFormatter.setMaximumFractionDigits(maximumFractionDigits);
        decimalFormatter.setGroupingUsed(false);
        decimalFormatter.setDecimalFormatSymbols(decimalFormatSymbols);
        return decimalFormatter;
    }
}
