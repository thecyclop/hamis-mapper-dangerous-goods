package com.portbase.bezoekschip.appcommon.visit;

import com.portbase.bezoekschip.appcommon.XmlUtils;
import com.portbase.bezoekschip.common.api.visit.ApiVisit;
import com.samskivert.mustache.Mustache;
import lombok.Value;
import lombok.experimental.Delegate;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Objects;

import static java.math.RoundingMode.HALF_UP;

@Slf4j
public abstract class VisitModel {

    @Delegate
    protected abstract ApiVisit getApiVisit();

    protected String doNotRemove() {
        return XmlUtils.DO_NOT_REMOVE_ATTRIBUTE;
    }

    protected Mustache.Lambda chopToSize = ((frag, out) -> {
        TemplateValue value = new TemplateValue(frag.execute());
        if ("".equals(value.rawValue)) {
            out.write("");
            return;
        }
        int maxSize = Integer.valueOf(value.params);
        String result = value.rawValue;
        if (result.length() > maxSize) {
            result = value.rawValue.substring(0, maxSize);
            log.info("Crn: {}, {}: Chopped some characters, originalValue: {}, result: {}, for markup: {}",
                    getCrn(), this.getClass().getSimpleName(), value.rawValue, result, frag.decompile());
        }
        out.write(result);
    });

    protected Mustache.Lambda chopDecimal = (frag, out) -> {
        TemplateValue value = new TemplateValue(frag.execute());
        if ("".equals(value.rawValue)) {
            out.write("");
            return;
        }
        int maxDigits = Integer.valueOf(value.params.split("\\.")[0].trim());
        int maxDecimals = Integer.valueOf((value.params + ".0").split("\\.")[1].trim());
        BigDecimal bigDecimal = new BigDecimal(value.rawValue, new MathContext(maxDigits, HALF_UP)).setScale(maxDecimals, HALF_UP);
        BigDecimal max = new BigDecimal(10).pow(maxDigits).subtract(BigDecimal.ONE);
        String result = (bigDecimal.compareTo(max) > 0 ? max : bigDecimal).stripTrailingZeros().toPlainString();
        if (!Objects.equals(result, value.rawValue)) {
            log.info("Crn: {}, {}: Chopped decimal number, originalValue: {}, choppedResult: {}, for markup: {}",
                    getCrn(), this.getClass().getSimpleName(), value.rawValue, result, frag.decompile());
        }
        out.write(result);
    };

    @Value
    private static class TemplateValue {
        String rawValue, params;

        private TemplateValue(String templateValue) {
            int splitPosition = templateValue.lastIndexOf("|");
            if (splitPosition < 0) {
                throw new IllegalStateException("Failed to find split symbol in mustache template for value: " + templateValue);
            }
            this.rawValue = templateValue.substring(0, splitPosition).trim();
            this.params = templateValue.substring(splitPosition + 1).trim();
        }
    }
}
