package com.portbase.bezoekschip.pasync;

import com.portbase.bezoekschip.common.api.dangerousgoods.common.Fumigant;
import com.portbase.bezoekschip.common.api.dangerousgoods.common.PackageCategory;
import com.portbase.bezoekschip.common.api.dangerousgoods.common.PackingGroup;
import com.portbase.bezoekschip.common.api.dangerousgoods.common.TransportIndex;
import com.samskivert.mustache.Mustache;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import static com.portbase.bezoekschip.appcommon.MapperUtils.decimalFormatter;
import static com.portbase.bezoekschip.appcommon.XmlUtils.*;
import static java.lang.System.getProperty;

@Slf4j
public class PaUtils {
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ssXXX");

    private static final DecimalFormat decimalFormat = decimalFormatter(0, 10);

    public static String createMessage(String templateFile, Object model) {
        return createXmlMessage(computeTemplate(templateFile, PaUtils::compiler), model);
    }

    public static boolean shouldSendToPa(String crn) {
        String environment = getProperty("environment");
        if ((crn.startsWith("NLRTM") || crn.startsWith("NLAMS"))
                && ("prod".equals(environment) || "kt".equals(environment) || "sc".equals(environment))){
            log.info("Not sending declarations to hamis (crn {})", crn);
            return false;
        }
        return true;
    }

    public static Mustache.Compiler compiler() {
        return XML_COMPILER.withFormatter(value -> {
            if (value instanceof Instant) {
                return dateTimeFormatter.format(((Instant) value).atZone(ZoneId.of("Europe/Amsterdam")));
            }  else if (value instanceof BigDecimal || value instanceof Long) {
                return decimalFormat.format(value);
            } else if (value instanceof PackingGroup) {
                switch ((PackingGroup) value) {
                    case GREAT_DANGER:
                        return "1";
                    case MEDIUM_DANGER:
                        return "2";
                    case MINOR_DANGER:
                        return "3";
                }
            } else if (value instanceof PackageCategory) {
                switch ((PackageCategory) value) {
                    case CATEGORY_1:
                        return "1";
                    case CATEGORY_2:
                        return "2";
                    case CATEGORY_3:
                        return "3";
                }
            } else if (value instanceof TransportIndex) {
                switch ((TransportIndex) value) {
                    case II:
                        return "2";
                    case III:
                        return "3";
                }
            } else if (value instanceof Fumigant) {
                switch ((Fumigant) value) {
                    case AMMONIA:
                        return "AMM";
                    case CHLOROPICRIN:
                        return "CHL";
                    case CO:
                        return "CO";
                    case CO2:
                        return "CO2";
                    case PHOSPHINE:
                        return "FOS";
                    case FORMALDEHYDE:
                        return "FOR";
                    case METHYLBROMIDE:
                        return "MET";
                    case SULFURYL_FLUORIDE:
                        return "SUL";
                    case N2:
                        return "N2";
                }
            }
            return value.toString();
        }).defaultValue("");
    }
}
