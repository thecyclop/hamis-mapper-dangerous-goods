package com.portbase.bezoekschip.pasync.dangerousgoods;

import com.portbase.bezoekschip.appcommon.messaging.EmhHeaderModel;
import com.portbase.bezoekschip.appcommon.visit.VisitModel;
import com.portbase.bezoekschip.common.api.common.authentication.Organisation;
import com.portbase.bezoekschip.common.api.common.visit.BerthVisit;
import com.portbase.bezoekschip.common.api.dangerousgoods.common.*;
import com.portbase.bezoekschip.common.api.visit.ApiVisit;
import lombok.Value;
import lombok.experimental.Delegate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import static com.portbase.bezoekschip.pasync.PaUtils.createMessage;
import static java.lang.Math.pow;
import static java.lang.Math.round;
import static java.util.Optional.of;
import static java.util.Optional.ofNullable;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Stream.concat;

@Value
public class PaDangerousGoodsModel extends VisitModel {
    public static final String TYPE = "mst.dangerousgoodsdeclaration";
    public static final String MOERDIJK_TYPE = "mst.dangerousgoods.dgdeclaration";
    public static final String RESPONSE_QUEUE = "jms/portbase/topic/portvisit/dg2pa/publish";

    private static final String TEMPLATE_FILE = "dangerousgoods/m204-dg2pa.mustache";

    ApiVisit apiVisit;
    DangerousGoods apiDangerousGoods;
    Organisation sender;
    boolean replace;

    Map<String, Good> goods;
    Map<String, BerthVisit> berthMap;

    public PaDangerousGoodsModel(ApiVisit apiVisit, boolean replace) {
        this.apiVisit = apiVisit;
        this.replace = replace;
        this.apiDangerousGoods = apiVisit.getDangerousGoodsDeclaration();
        this.sender = this.apiDangerousGoods.getSender();

        goods = apiDangerousGoods.getGoods().stream().collect(toMap(Good::getId, identity()));
        berthMap = apiVisit.getVisitDeclaration().getPortVisit().getBerthVisits().stream()
                .collect(toMap(BerthVisit::getId, identity()));
    }

    public String toString() {
        return EmhHeaderModel.builder()
                .receiver(apiVisit.getPortOfCall().getPortAuthority().getShortName())
                .crn(apiVisit.getCrn())
                .goal(replace ? "replace" : "original")
                .payload(createMessage(TEMPLATE_FILE, this))
                .type(TYPE)
                .smallType("dg2pa")
                .responseQueue(RESPONSE_QUEUE)
                .build().toString();
    }


    public boolean getContainsBulk() {
        return apiDangerousGoods.getStowageAtArrival().stream().anyMatch(stowage -> "tank".equals(stowage.getType()));
    }

    public List<StowageHandling> getConsignments() {
        Stream<StowageHandling> transit = apiDangerousGoods.getTransitStowage().stream()
                .map(stowage -> new StowageHandling(null, stowage));

        Stream<StowageHandling> handlingConsignments = apiDangerousGoods.getHandlings().stream()
                .flatMap(handling -> handling.getAction().stream().map(s -> new StowageHandling(handling, s)));
        return concat(transit, handlingConsignments).collect(toList());
    }

    @Value
    public class StowageHandling {
        Handling handling;
        @Delegate
        Stowage stowage;
        Good good;
        BerthVisit berthVisit;

        public StowageHandling(Handling handling, Stowage stowage) {
            this.handling = handling;
            this.stowage = stowage;
            this.good = goods.get(stowage.getGoodId());
            this.berthVisit = handling == null ? null : berthMap.get(handling.getBerthVisitId());
        }

        String getHandlingType() {
            if (handling == null) {
                return "T";
            }
            switch (handling.getType()) {
                case "discharge":
                    return "LDI";
                case "washing":
                case "ventilation":
                    return "CTC";
                case "inerting":
                    return "CTC";
                case "blending":
                case "loading":
                    return "LLO";
            }
            return null;
        }

        public String getWeightUnit() {
            return stowage instanceof BreakBulk || stowage instanceof Container ? "KGM" : "TNE";
        }


        public BigDecimal getRoundedWeight() {
            return stowage.getWeight() == null ? null : stowage.getWeight().setScale(0, RoundingMode.HALF_UP);
        }

        public BigDecimal getRoundedNetWeight() {
            BigDecimal weight = stowage instanceof BreakBulk ? ((BreakBulk) stowage).getNetWeight() :
                    stowage instanceof Container ? ((Container) stowage).getNetWeight() : null;

            return weight == null ? null : weight.setScale(0, RoundingMode.HALF_UP);
        }

        public BigDecimal getRoundedNetExplosiveMass() {
            BigDecimal weight = stowage instanceof BreakBulk ? ((BreakBulk) stowage).getNetExplosiveMass() :
                    stowage instanceof Container ? ((Container) stowage).getNetExplosiveMass() : null;

            return weight == null ? null : weight.setScale(0, RoundingMode.HALF_UP);
        }

        public RadioActivityView getRadioActivityView() {
            if (stowage instanceof Container) {
                Radioactivity radioactivity = ((Container) stowage).getRadioactivity();
                if (radioactivity.getLevel() != null) {

                    //Let op: in het veld kunnen maar 5 cijfers staan, vandaar deze if boom
                    if (radioactivity.getLevel() >= round(pow(10, 14))) {
                        long teraBecquerel = radioactivity.getLevel() / round(pow(10, 12));
                        return new RadioActivityView(teraBecquerel >= round(pow(10, 5)) ? "99999" : teraBecquerel + "", "TBQ");
                    } else if (radioactivity.getLevel() >= round(pow(10, 11))) {
                        return new RadioActivityView((radioactivity.getLevel() / round(pow(10, 9))) + "", "GBQ");
                    } else if (radioactivity.getLevel() >= round(pow(10, 5))) {
                        return new RadioActivityView((radioactivity.getLevel() / round(pow(10, 6))) + "", "4N");
                    } else {
                        return new RadioActivityView(radioactivity.getLevel() + "", "BQL");
                    }
                }
            }
            return null;
        }


        public boolean isContainerOrBreakBulk() {return stowage instanceof Container || stowage instanceof BreakBulk;}

        public Container getContainer() {
            return stowage instanceof Container ? (Container) stowage : null;
        }
        public BreakBulk getBreakBulk() {
            return stowage instanceof BreakBulk ? (BreakBulk) stowage : null;
        }

        public Fluid getFluid() {
            return good instanceof Fluid ? (Fluid) good : null;
        }

        public Solid getSolid() {
            return good instanceof Solid ? (Solid) good : null;
        }

        public Chemical getChemical() {
            return good instanceof Chemical ? (Chemical) good : null;
        }

        public Tank getTank() {
            return stowage instanceof Tank ? (Tank) stowage : null;
        }

        public Handling getCleaning() {
            return handling instanceof Washing || handling instanceof Ventilation ? handling : null;
        }

        public Washing getWashing() {
            return handling instanceof Washing ? (Washing) handling : null;
        }

        public Blending getBlending() {
            return handling instanceof Blending ? (Blending) handling : null;
        }

        public String getCleaningMethod() {
            return Optional.ofNullable(getWashing()).map(w -> w.getMediumOrCommercialWash() ? "Wash / Medium" : "Prewash").orElse(null);
        }

        public String getCleaningRemarks() {
            if (handling instanceof Washing) {
                return ((Washing) handling).getRemarks();
            }
            if (handling instanceof Inerting) {
                return "INERTING";
            }
            if (handling instanceof Ventilation) {
                return "VENTILATION" + Optional.ofNullable(((Ventilation) handling).getRemarks())
                        .map(r -> " – " + r).orElse("");
            }
            return null;
        }

        public String getWashwaterDestination() {
            return Optional.ofNullable(getWashing()).map(w -> {
                if (w.getOtherDestination() != null) {
                    switch (w.getOtherDestination()) {
                        case OTHER_CARGO_TANK:
                        case SAME_CARGO_TANK:
                        case SLOP_TANK:
                            return "On board";
                    }
                    return "Other";
                }
                return null;
            }).orElse(null);
        }

        public String getEmptyContainerSpecification() {
            if (stowage instanceof Container) {
                return ((Container) stowage).isUncleanTankContainer() ? "empty_not_gas_free" : null; // conform MGS1.0, should, in fact, be empty
            }
            if (stowage instanceof Tank) {
                return of(((Tank) stowage).getTankStatus())
                        .map(status -> {
                            switch (status) {
                                case EMPTY_INERT_NOT_GAS_FREE:
                                    return "empty_inert";
                                case EMPTY_NOT_GAS_FREE:
                                    return "empty_not_gas_free";
                                case RESIDUE_INERT:
                                    return "residue_inert";
                                case RESIDUE:
                                    return "residue_not_gas_free";
                            }
                            return null;
                        }).orElse(null);
            }
            return null;
        }

        public String getImoCode() {
            switch (good.getType()) {
                case "containerGood":
                    return "IMDG";
                case "solid":
                    return "IMSBC";
                case "oil":
                    return "MARPOLANNEX1";
                case "gas":
                    return "IGC";
                case "chemical":
                    return "IBC";
                default:
                    return null;
            }
        }

        public String getMarpolAnnex() {
            if (good == null) {
                return null;
            }
            switch (good.getType()) {
                case "oil":
                    return "I";
                case "gas":
                    return "II";
                case "chemical":
                    return "II";
                default:
                    return null;
            }
        }

        public String getConvertedBerthCode() {
            return ofNullable(berthVisit.getBerth().getCode()).orElse(null);
        }

        public boolean isMarinePollutant() {
            return getFluid() != null || (good instanceof ContainerGood &&
                    ofNullable(((ContainerGood) good).getGoodData().getSubsidiaryRisk())
                            .map(risk -> risk.contains("P")).orElse(false));
        }

        public String getUnCode() {
            if (good == null) {
                return "0000";
            }
            String unCode;
            switch (good.getType()) {
                case "containerGood":
                    unCode = ((ContainerGood) good).getGoodData().getUnCode();
                    return unCode == null ? "0000" : unCode;
                case "solid":
                    unCode = ((Solid) good).getGoodData().getUnCode();
                    return unCode == null ? "0000" : unCode;
                case "gas":
                    unCode = ((Gas) good).getGoodData().getUnCode();
                    return unCode == null ? "0000" : unCode;
                default:
                    return "0000";
            }
        }

        public String getImoClass() {
            if (good == null) {
                return "0.0";
            }
            switch (good.getType()) {
                case "containerGood":
                    return ((ContainerGood) good).getGoodData().getHazardClass();
                case "solid":
                    return ((Solid) good).getGoodData().getHazardClass();
                default:
                    return "0.0";
            }
        }


        public Instant handlingTime() {
            if (handling != null) {
                switch (handling.getType()) {
                    case "washing":
                        return ((Washing) handling).getTimestamp();
                    case "ventilation":
                        return ((Ventilation) handling).getTimestamp();
                    case "blending":
                    case "loading":
                        return berthMap.get(handling.getBerthVisitId()).getEtd();
                    case "inerting":
                    case "discharge":
                        return berthMap.get(handling.getBerthVisitId()).getEta();
                }
            }
            return null;
        }

        public String getSubCarriageType() {
            if (handling != null) {
                if (handling instanceof Loading) {
                    return ((Loading) handling).isShipToShip() ? "10" : null;
                }
                if (handling instanceof Blending) {
                    return ((Blending) handling).isShipToShip() ? "10" : null;
                }
                if (handling instanceof Discharge) {
                    return ((Discharge) handling).isShipToShip() ? "30" : null;
                }
            }
            return null;
        }

    }


    @Value
    public static class RadioActivityView {
        String normalizedLevel;
        String unit;
    }

}
