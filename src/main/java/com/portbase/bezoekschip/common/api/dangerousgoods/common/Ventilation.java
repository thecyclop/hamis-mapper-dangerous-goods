package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Value;
import lombok.experimental.Wither;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

import static com.portbase.bezoekschip.common.api.dangerousgoods.common.TankStatus.EMPTY;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

@Value
public class Ventilation implements Handling {

    @ApiModelProperty(value = "The berth where the ventilation of the tank or hold is performed")
    String berthVisitId;

    @ApiModelProperty(value = "The identifiers of tanks selected for ventilation")
    @NotNull @NotEmpty List<@NotBlank String> tankNumbers;

    @ApiModelProperty(value = "The date and time when the ventilation of the tank is performed at the berth")
    @NotNull Instant timestamp;

    @ApiModelProperty(value = "The remarks about the ventilation of the tank at the berth")
    String remarks;

    @Override
    public String getType() {
        return "ventilation";
    }

    @Wither
    List<Stowage> stowageBefore;

    @Override
    public List<Stowage> getAction() {
        return stowageBefore.stream()
                .filter(s -> s instanceof Tank && tankNumbers.contains(s.getStowageNumber()))
                .map(s -> ((Tank) s).toBuilder().tankStatus(EMPTY).build()).collect(toList());
    }

    @Null(message = "Stowage cannot be ventilated: ${validatedValue}")
    private String getHandlingErrors() {
        StringBuilder errorBuilder = new StringBuilder();
        tankNumbers.forEach(s -> {
            Tank existing = (Tank) stowageBefore.stream()
                    .filter(e -> e instanceof Tank && e.getStowageNumber().equals(s))
                    .findFirst().orElse(null);
            if (existing == null) {
                errorBuilder.append(format("tank %s is missing", s));
                return;
            }
            if (existing.getWeight().compareTo(BigDecimal.ZERO) > 0) {
                errorBuilder.append(format("tank %s is not empty", s));
            }
        });
        String error = errorBuilder.toString();
        return error.isEmpty() ? null : error;
    }
}
