package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;

@Value
@Builder(toBuilder = true)
public class Fumigation {

    @ApiModelProperty(value = "The name of the fumigant")
    @NotNull Fumigant fumigant;

    @ApiModelProperty(value = "Date and time of fumigation of dangerous goods")
    Instant fumigationTimestamp;

    @ApiModelProperty(value = "Indication whether measuring equipment is available")
    Boolean measurementEquipment;

    @ApiModelProperty(value = "The berth where the dangerous goods will be fumigated")
    String placeOfFumigation;

    @ApiModelProperty(value = "The amount of fumigant in parts per million (PPM)")
    @Digits(integer = 3, fraction = 1) BigDecimal amount;

    Boolean ventilated;
}
