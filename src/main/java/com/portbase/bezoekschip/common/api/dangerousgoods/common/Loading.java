package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Singular;
import lombok.Value;
import lombok.experimental.Wither;
import lombok.extern.slf4j.Slf4j;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

import static java.lang.String.format;
import static java.util.UUID.randomUUID;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Stream.concat;

@Value
@Builder(toBuilder = true)
@Slf4j
public class Loading implements Handling {

    @ApiModelProperty(value = "The berth where the loading is performed")
    String berthVisitId;

    @Singular
    @NotNull List<@NotNull @Valid Stowage> stowages;

    @ApiModelProperty(value = "Indication whether the loading is performed from ship to ship. Only applicable for gas, oil and chemical")
    boolean shipToShip;

    @ApiModelProperty(value = "Indication whether this handling will be performed while keeping the tanks inert")
    Boolean keepInert;

    @Override
    public String getType() {
        return "loading";
    }

    @Wither
    List<Stowage> stowageBefore;

    @Override
    public List<Stowage> getAction() {
        return stowages;
    }

    @JsonIgnore
    @Override
    public List<Stowage> getStowageAfter() {
        return new ArrayList<>(concat(getStowageBefore().stream(), getAction().stream()).collect(
                toMap(s -> s.isBulk() ? s.getType() + s.getStowageNumber() : randomUUID().toString(),
                      identity(), (a, b) -> b.withWeight(a.getWeight().add(b.getWeight())),
                      LinkedHashMap::new)).values());
    }

    @Null(message = "Stowage cannot be loaded: ${validatedValue}")
    private String getHandlingErrors() {
        StringBuilder errorBuilder = new StringBuilder();
        stowages.forEach(s -> {
            Stowage existing = stowageBefore.stream()
                    .filter(s1 -> Objects.equals(s1.getStowageNumber(), s.getStowageNumber()) && Objects
                            .equals(s1.getType(), s.getType())).findFirst().orElse(null);
            switch (s.getType()) {
                case "tank":
                    if (existing == null) {
                        errorBuilder.append(format("tank %s is not reported at arrival", s.getStowageNumber()));
                    }
                    break;
                case "container":
                case "breakBulk":
                    if (existing != null) {
                        errorBuilder.append(format("%s is already present", existing.getStowageNumber()));
                    }
                    break;
            }
            if (existing != null && existing.getWeight().compareTo(BigDecimal.ZERO) > 0) {
                if (!existing.getGoodId().equals(s.getGoodId())) {
                    errorBuilder.append(format("%s already contains another good", existing.getStowageNumber()));
                }
                if (existing instanceof Tank && ((Tank) existing).getTankStatus().isInert()) {
                    if (keepInert == null) {
                        errorBuilder.append("required to indicate whether tanks remain inert");
                    } else if (keepInert == Boolean.TRUE && ((Tank) s).getTankStatus() != TankStatus.NOT_EMPTY_INERT) {
                        errorBuilder.append(String.format("tank %s should remain inert", s.getStowageNumber()));
                    } else if (keepInert == Boolean.FALSE && ((Tank) s).getTankStatus() != TankStatus.NOT_EMPTY) {
                        errorBuilder.append(String.format("tank %s should not remain inert", s.getStowageNumber()));
                    }
                }
            }
        });
        String error = errorBuilder.toString();
        return error.isEmpty() ? null : error;
    }
}
