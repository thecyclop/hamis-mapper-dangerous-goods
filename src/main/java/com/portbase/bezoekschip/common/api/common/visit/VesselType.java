package com.portbase.bezoekschip.common.api.common.visit;

import lombok.Value;

@Value
public class VesselType {
    String code;
    String name;
}
