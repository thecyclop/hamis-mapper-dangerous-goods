package com.portbase.bezoekschip.common.api.common.visit;


import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Singular;
import lombok.Value;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;

import static java.lang.Boolean.FALSE;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Value
@Builder(toBuilder = true, builderClassName = "Builder")
@ApiModel(description = "The port visit of the vessel to the port of call")
public class PortVisit {
    @ApiModelProperty(value = "The estimated time of arrival of the vessel at the pilot station or entry point")
    @NotNull Instant etaPort;

    @Singular
    @ApiModelProperty(value = "The berth visit of the vessel")
    @NotNull List<@Valid BerthVisit> berthVisits;

    @ApiModelProperty(value = "The estimated time of departure of the vessel from the exit point")
    @NotNull Instant etdPort;
}