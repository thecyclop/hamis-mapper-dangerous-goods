package com.portbase.bezoekschip.common.api.common.visit;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Value;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Value
@Builder(toBuilder = true, builderClassName = "Builder")
@ApiModel(description = "The declaration of the port visit")
public class VisitDeclaration {
    @NotNull @Valid PortVisit portVisit;
}
