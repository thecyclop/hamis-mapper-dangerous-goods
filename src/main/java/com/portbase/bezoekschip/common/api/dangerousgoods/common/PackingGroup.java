package com.portbase.bezoekschip.common.api.dangerousgoods.common;

public enum PackingGroup {
    MINOR_DANGER, MEDIUM_DANGER, GREAT_DANGER;
}
