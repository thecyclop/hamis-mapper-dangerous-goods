package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import lombok.Value;

@Value
public class PackageType {
    String code;
    String name;
}
