package com.portbase.bezoekschip.common.api.dangerousgoods.common;


public enum Fumigant {
    AMMONIA, CO, CO2, CHLOROPICRIN, FORMALDEHYDE, METHYLBROMIDE, PHOSPHINE, SULFURYL_FLUORIDE, N2;
}