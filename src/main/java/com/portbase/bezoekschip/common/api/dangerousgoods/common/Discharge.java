package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Singular;
import lombok.Value;
import lombok.experimental.Wither;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

import static com.portbase.bezoekschip.common.api.dangerousgoods.common.TankStatus.RESIDUE;
import static com.portbase.bezoekschip.common.api.dangerousgoods.common.TankStatus.RESIDUE_INERT;
import static java.lang.String.format;
import static java.math.BigDecimal.ZERO;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Value
@Builder(toBuilder = true)
public class Discharge implements Handling {

    @ApiModelProperty(value = "The berth where the discharge is performed")
    String berthVisitId;

    @Singular
    @NotNull @NotEmpty List<@NotNull @Valid DischargedStowage> stowages;

    @ApiModelProperty(value = "Indication whether the discharge is performed from ship to ship. Only applicable for gas, oil and chemical")
    boolean shipToShip;

    @ApiModelProperty(value = "Indication whether this handling will be performed while keeping the tanks inert")
    Boolean keepInert;

    @Override
    public String getType() {
        return "discharge";
    }

    @Wither
    List<Stowage> stowageBefore;

    @Override
    public List<Stowage> getAction() {
        Map<String, DischargedStowage> dischargedStowage =
                stowages.stream().collect(toMap(d -> d.getType() + d.getStowageNumber(), identity()));
        return getStowageBefore().stream()
                .filter(s -> dischargedStowage.containsKey(s.getType() + s.getStowageNumber()))
                .flatMap(s -> discharge(s, dischargedStowage.get(s.getType() + s.getStowageNumber())))
                .collect(toList());
    }

    private Stream<Stowage> discharge(Stowage stowage, DischargedStowage discharge) {
        BigDecimal weight = discharge.getEmptied() ? stowage.getWeight() : discharge.getAmount();
        BigDecimal remaining = stowage.getWeight().subtract(weight);
        switch (stowage.getType()) {
            case "tank":
                Tank tank = (Tank) stowage;
                TankStatus tankStatus = tank.getTankStatus().keepInert(keepInert);
                return Stream.of(tank.toBuilder()
                        .weight(weight)
                        .tankStatus(remaining.compareTo(ZERO) > 0 ? tankStatus : tankStatus.isInert() ? RESIDUE_INERT : RESIDUE)
                        .build());
            case "hold":
                return Stream.of(((Hold) stowage).toBuilder()
                        .weight(weight)
                        .build());
        }
        return Stream.of(stowage);
    }

    @JsonIgnore
    @Override
    public List<Stowage> getStowageAfter() {
        Map<String, Stowage> actionMap = getAction().stream().collect(
                toMap(s -> s.getType() + s.getStowageNumber(), identity(), (a, b) -> b));
        return getStowageBefore().stream().flatMap(a -> {
            Stowage b = actionMap.get(a.getType() + a.getStowageNumber());
            if (b == null) {
                return Stream.of(a);
            }
            BigDecimal remaining = a.getWeight().subtract(b.getWeight());
            switch (a.getType()) {
                case "hold":
                    return remaining.compareTo(ZERO) > 0 ? Stream.of(b.withWeight(remaining)) : Stream.empty();
                case "tank":
                    return Stream.of(b.withWeight(remaining));
                default:
                    return Stream.empty();
            }
        }).collect(toList());
    }

    @Null(message = "Stowage cannot be discharged: ${validatedValue}")
    private String getHandlingErrors() {
        StringBuilder errorBuilder = new StringBuilder();
        stowages.forEach(d -> {
            Stowage existing = stowageBefore.stream().filter(s -> Objects.equals(s.getType(), d.getType()) && Objects
                    .equals(s.getStowageNumber(), d.getStowageNumber())).findFirst().orElse(null);
            if (existing == null) {
                errorBuilder.append(format("stowage %s is missing", d.getStowageNumber()));
                return;
            }
            if (!d.getEmptied() && existing.getWeight().compareTo(d.getAmount()) < 0) {
                errorBuilder.append(format("stowage %s contains less than the discharged amount",
                        d.getStowageNumber()));
            }
            if (!d.getEmptied() && !existing.isBulk()) {
                errorBuilder.append(format("stowage %s cannot be partly discharged", d.getStowageNumber()));
            }
            if (keepInert == null && existing instanceof Tank && ((Tank) existing).isInert()) {
                errorBuilder.append("required to indicate whether tanks remain inert");
            }
        });
        String error = errorBuilder.toString();
        return error.isEmpty() ? null : error;
    }
}
