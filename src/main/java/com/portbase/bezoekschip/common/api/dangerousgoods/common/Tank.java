package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.portbase.bezoekschip.common.api.common.visit.Port;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Value
@Builder(toBuilder = true)
public class Tank implements Stowage {
    String stowageNumber;
    String goodId;
    Port portOfLoading;
    Port portOfDischarge;
    @ApiModelProperty(value = "The gross weight of the liquid in the tank (TNE)")
    @Wither
    BigDecimal weight;

    @NotNull TankStatus tankStatus;

    @JsonIgnore
    boolean isClean() {
        switch (tankStatus) {
            case EMPTY:
            case EMPTY_NOT_GAS_FREE:
            case EMPTY_INERT_NOT_GAS_FREE:
            case EMPTY_INERT:
                return true;
        }
        return false;
    }

    @JsonIgnore
    boolean isInert() {
        switch (tankStatus) {
            case EMPTY_INERT_NOT_GAS_FREE:
            case EMPTY_INERT:
            case RESIDUE_INERT:
            case NOT_EMPTY_INERT:
                return true;
        }
        return false;
    }

    @Override
    public String getType() {
        return "tank";
    }
}
