package com.portbase.bezoekschip.common.api.common.visit;

import com.portbase.bezoekschip.common.api.common.authentication.Organisation;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Value;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Value
@Builder(toBuilder = true, builderClassName = "Builder")
@ApiModel(description = "The port of call for the visit")
public class PortOfCall {

    @NotNull @Valid Port port;

    @NotNull @Valid Organisation portAuthority;
}
