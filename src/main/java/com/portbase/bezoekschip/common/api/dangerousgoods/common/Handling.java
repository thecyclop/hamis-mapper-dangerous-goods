package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.util.UUID.randomUUID;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Stream.concat;
import static org.slf4j.LoggerFactory.getLogger;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Discharge.class, name = "discharge"),
        @JsonSubTypes.Type(value = Washing.class, name = "washing"),
        @JsonSubTypes.Type(value = Ventilation.class, name = "ventilation"),
        @JsonSubTypes.Type(value = Inerting.class, name = "inerting"),
        @JsonSubTypes.Type(value = Loading.class, name = "loading"),
        @JsonSubTypes.Type(value = Blending.class, name = "blending"),
})
public interface Handling {
    @ApiModelProperty(value = "The berth where the handling is performed")
    @NotNull @NotBlank String getBerthVisitId();

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotBlank String getType();

    //these are calculated on the fly when DangerousGoods is created to help with validation and other stuff

    @JsonIgnore
    @NotNull List<Stowage> getStowageBefore();

    Handling withStowageBefore(List<Stowage> stowageBefore);

    @JsonIgnore
    default Map<String, Stowage> getBulkBefore() {
        return getStowageBefore().stream().filter(Stowage::isBulk)
                .collect(toMap(Stowage::getStowageNumber, identity(), (a, b) -> {
                    getLogger(getClass()).warn("Conflicting bulk was found at the same position: 1: {}, 2: {}", a, b);
                    return b;
                }));
    }

    @JsonIgnore
    List<Stowage> getAction();

    @JsonIgnore
    default List<Stowage> getStowageAfter() {
        return new ArrayList<>(concat(getStowageBefore().stream(), getAction().stream()).collect(
                toMap(s -> s.isBulk() ? s.getType() + s.getStowageNumber() : randomUUID().toString(),
                      identity(), (a, b) -> b, LinkedHashMap::new)).values());
    }
}
