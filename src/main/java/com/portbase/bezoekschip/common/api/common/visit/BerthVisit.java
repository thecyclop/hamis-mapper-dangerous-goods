package com.portbase.bezoekschip.common.api.common.visit;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.SortedSet;

@Value
@Builder(toBuilder = true, builderClassName = "Builder")
@ApiModel(description = "The berth visit. nextMovement = The local movement that starts from the berth. " +
        "boatmenAtArrival = The boatmen service for arrival at the berth. " +
        "boatmenAtDeparture = The boatmen service for departure from the berth. ")
public class BerthVisit {
    @ApiModelProperty(value = "The identifier of the berth visit")
    @NotNull @NotBlank String id;

    @Wither
    @NotNull @Valid Berth berth;

    @ApiModelProperty(value = "The estimated time of arrival of the vessel at the berth")
    @Wither
    Instant eta;

    @ApiModelProperty(value = "The estimated time of departure of the vessel from the berth")
    @Wither
    Instant etd;
}
