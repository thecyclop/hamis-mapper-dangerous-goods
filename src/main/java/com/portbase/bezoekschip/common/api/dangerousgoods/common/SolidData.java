package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;

/**
 * Also known as IMSBC
 */
@Value
@Builder
public class SolidData implements GoodData {

    @ApiModelProperty(value = "The name of the dangerous good")
    String name;

    @ApiModelProperty (value = "The UN code of the dangerous good")
    String unCode;

    @ApiModelProperty(value = "The IMO hazard class of the dangerous good")
    String hazardClass;

    @ApiModelProperty (value = "Indication if goodData originates from EMSA (European Maritime Safety Agency")
    boolean emsaGoodData;

    @Override
    public String getType() {
        return "solid";
    }

    @Override
    public String getStowageType() {
        return "hold";
    }
}
