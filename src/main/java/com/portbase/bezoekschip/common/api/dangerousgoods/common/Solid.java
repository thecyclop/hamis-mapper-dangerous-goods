package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Value;

@Value
public class Solid implements Good {
    String id;
    
    @ApiModelProperty(reference = "#/definitions/GoodData")
    SolidData goodData;
    String segregationInformation;
    String remarks;

    @Override
    public String getType() {
        return "solid";
    }
}
