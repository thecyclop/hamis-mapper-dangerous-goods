package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;

/**
 * Also known as MARPOL ANNEX 1
 */
@Value
@Builder
public class OilData implements FluidData {

    @ApiModelProperty(value = "The name of the dangerous good")
    String name;

    @ApiModelProperty(value = "The GDS code of the dangerous good")
    String gdsCode;

    @ApiModelProperty (value = "Indication if goodData originates from EMSA (European Maritime Safety Agency")
    boolean emsaGoodData;

    @Override
    public String getType() {
        return "oil";
    }
}
