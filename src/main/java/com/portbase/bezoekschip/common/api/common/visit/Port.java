package com.portbase.bezoekschip.common.api.common.visit;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Value
@Builder(toBuilder = true, builderClassName = "Builder")
public class Port {

    @ApiModelProperty(value = "The name of the port")
    @NotNull @NotBlank String name;

    @ApiModelProperty(value = "The UN locode of the location where the port is located")
    @NotNull @NotBlank String locationUnCode;

    @ApiModelProperty(value = "The UN code of the country where the port is located")
    @NotNull @NotBlank String countryUnCode;

    @ApiModelProperty(value = "Indication whether the port is located in the EU")
    boolean euPort;
}
