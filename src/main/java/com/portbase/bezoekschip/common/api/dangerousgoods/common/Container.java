package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import com.portbase.bezoekschip.common.api.common.visit.Port;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Value
@Builder(toBuilder = true)
public class Container implements Stowage {
    String stowageNumber;
    String goodId;
    Port portOfLoading;
    Port portOfDischarge;
    @ApiModelProperty(value = "The gross weight of the goods in the container (kg)")
    @Wither
    BigDecimal weight;

    boolean trailer;
    boolean uncleanTankContainer;

    @NotBlank String position;
    Integer numberOfOuterPackages;
    @Valid PackageType outerPackageType;
    @PositiveOrZero Integer numberOfInnerPackages;
    @Valid PackageType innerPackageType; // verplicht als je meer dan 1 hebt
    boolean transportInLimitedQuantity;
    @ApiModelProperty(value = "The net weight of the cargo (kg)")
    BigDecimal netWeight; //[10,3]
    @ApiModelProperty(value = "The net explosive mass of the cargo (kg)")
    BigDecimal netExplosiveMass;

    boolean fumigated;
    @Valid Fumigation fumigation;

    @Valid Radioactivity radioactivity;

    @Override
    public String getType() {
        return "container";
    }

    @AssertTrue(message = "Fumigation info is missing")
    private boolean isFumigationPresent() {
        return !fumigated || fumigation != null;
    }

    @AssertTrue(message = "Gross weight should be larger or equal than net weight")
    private boolean isGrossWeightMoreThanNetWeight() {
        return netWeight == null || weight.compareTo(netWeight) >= 0;
    }

    @AssertTrue(message = "Number of outer packages should be positive")
    private boolean isNumberOfOuterPackagesPositive() {
        return uncleanTankContainer || (numberOfOuterPackages != null && numberOfOuterPackages > 0);
    }

    @AssertTrue(message = "Number of outer packages is not allowed for unclean tankContainer")
    private boolean isNumberOfOuterPackagesNotAllowedWithUncleanTankContainer() {
        return !uncleanTankContainer || numberOfOuterPackages == null || numberOfOuterPackages == 0;
    }

}
