package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Value;

import java.math.BigDecimal;

@Value
public class Oil implements Fluid {
    String id;
    BigDecimal flashPoint;
    String remarks;
    
    @ApiModelProperty(reference = "#/definitions/GoodData")
    OilData goodData;

    @Override
    public String getType() {
        return "oil";
    }
}
