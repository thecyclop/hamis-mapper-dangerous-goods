package com.portbase.bezoekschip.common.api.common.visit;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Value
@Builder(toBuilder = true, builderClassName = "Builder")
@ApiModel(description = "The vessel that visits the port of call")
public class Vessel {

    @ApiModelProperty(value = "The unique identification of a vessel according to Lloyds register")
    @NotNull @NotBlank String imoCode;

    @ApiModelProperty(value = "The name of the vessel")
    @NotNull @NotBlank String name;

    @ApiModelProperty(value = "The communication id of the vessel that is used to communicate with the vessel by radio")
    @NotNull @NotBlank String radioCallSign;

    @ApiModelProperty(value = "The UN code of the means of transport")
    @NotNull @NotBlank String motUnCode;

    @ApiModelProperty(value = "The name of the means of transport")
    String motName;

    @ApiModelProperty(value = "The weight that can be carried by the vessel")
    Long summerDeadWeight;

    @ApiModelProperty(value = "The maximum width of the vessel")
    BigDecimal maxWidth;

    @ApiModelProperty(value = "The code of the flag that defines the nationality of a vessel as reported to port and/or customs authorities")
    String flagCode;

    @ApiModelProperty(value = "The UN code of the country that is related to the flag that defines the nationality of a vessel as reported to port and/or customs authorities")
    String flagCountryUnCode;

    @ApiModelProperty(value = "The net loading capacity of the vessel, expressed in tons")
    @NotNull BigDecimal netTonnage;

    @ApiModelProperty(value = "The gross loading capacity of the vessel, expressed in tons")
    @NotNull BigDecimal grossTonnage;

    @ApiModelProperty(value = "The UN locode of the location that is the registration place of the vessel")
    String registrationPlaceUnloCode;

    @ApiModelProperty(value = "The name of the location that is the registration place of the vessel")
    String registrationPlaceName;

    @ApiModelProperty(value = "The date the vessel was registered")
    @NotNull(message = "Vessel registration date is unknown")
    LocalDate registrationDate;

    @ApiModelProperty(value = "The MMSI number of the vessel")
    String mmsiNumber;

    @ApiModelProperty(value = "The full length of the vessel")
    @NotNull BigDecimal fullLength;

    @AssertTrue(message = "Vessel registration place is unknown")
    private boolean hasValidRegistrationLocation() {
        return !isBlank(registrationPlaceName) && !isBlank(registrationPlaceUnloCode);
    }

    @AssertTrue(message = "Vessel flag code is unknown")
    private boolean hasValidFlag() {
        return !isBlank(flagCode) && !isBlank(flagCountryUnCode);
    }
}
