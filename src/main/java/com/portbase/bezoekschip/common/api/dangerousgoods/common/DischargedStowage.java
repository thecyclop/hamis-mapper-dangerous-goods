package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Value
@Builder(toBuilder = true)
public class DischargedStowage {

    @ApiModelProperty(value = "Identifier of the tank, hold or container that is discharged at the berth")
    @NotNull @NotBlank String stowageNumber;

    @ApiModelProperty(value = "Type of stowage to discharge, i.e. tank, hold, container or breakBulk")
    @NotNull @NotBlank String type;

    @ApiModelProperty(value = "Indication whether the tank or hold is completely emptied at the berth")
    @NotNull Boolean emptied;

    @ApiModelProperty(value = "The amount that is discharged at the berth")
    @Positive BigDecimal amount;

    @AssertTrue(message = "Discharge amount is required for a partial discharge")
    private boolean hasAmountIfPartial() {
        return emptied || amount != null;
    }
}
