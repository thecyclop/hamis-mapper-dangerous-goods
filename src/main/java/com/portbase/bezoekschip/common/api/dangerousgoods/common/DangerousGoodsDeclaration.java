package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import com.portbase.bezoekschip.common.api.common.authentication.Organisation;
import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true, builderClassName = "Builder")
public class DangerousGoodsDeclaration {
    DangerousGoods dangerousGoods;
    Organisation cargoDeclarant;
    Organisation visitDeclarant;
    Organisation visitOwner;
    String upload;
    boolean completed;
    boolean declared;
    boolean declaredByVisitDeclarant;
}
