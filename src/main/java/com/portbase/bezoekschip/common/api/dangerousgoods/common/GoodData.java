package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import javax.validation.constraints.NotBlank;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = ContainerGoodData.class, name = "containerGood"),
        @JsonSubTypes.Type(value = SolidData.class, name = "solid"),
        @JsonSubTypes.Type(value = OilData.class, name = "oil"),
        @JsonSubTypes.Type(value = GasData.class, name = "gas"),
        @JsonSubTypes.Type(value = ChemicalData.class, name = "chemical")})
public interface GoodData {
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotBlank String getType();
    @NotBlank String getName();
    @NotBlank String getStowageType();
}
