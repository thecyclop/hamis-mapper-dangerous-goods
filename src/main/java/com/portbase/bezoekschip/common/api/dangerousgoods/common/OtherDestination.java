package com.portbase.bezoekschip.common.api.dangerousgoods.common;


public enum OtherDestination {
    SAME_CARGO_TANK, OTHER_CARGO_TANK, SLOP_TANK, OFF_BOARD
}