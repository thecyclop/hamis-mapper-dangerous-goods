package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;

@Value
@Builder(toBuilder = true)
public class ContainerGood implements Good {
    String id;

    @ApiModelProperty(reference = "#/definitions/GoodData")
    ContainerGoodData goodData;

    @Digits(integer = 3, fraction = 0)
    @ApiModelProperty (value = "The temperature in degrees Celsius at which a liquid will give off enough flammable vapour to be ignited. According IMDG Code DG Class 3 or subsidiary risk of Class 3")
    BigDecimal flashPoint;

    @ApiModelProperty (value = "Specified segregation information")
    String segregationInformation;

    @ApiModelProperty (value = "Any remarks about the container good")
    String remarks;

    boolean radioactive;

    @ApiModelProperty(value = "Class or division of radionuclide")
    @Valid Radionuclide radionuclide;

    @AssertTrue(message = "Radionuclide is missing")
    private boolean isRadionuclideReported() {
        return !radioactive || radionuclide != null;
    }

    @Override
    public String getType() {
        return "containerGood";
    }
}
