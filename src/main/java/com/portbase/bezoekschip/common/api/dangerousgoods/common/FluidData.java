package com.portbase.bezoekschip.common.api.dangerousgoods.common;

public interface FluidData extends GoodData {
    @Override
    default String getStowageType() {
        return "tank";
    }
}
