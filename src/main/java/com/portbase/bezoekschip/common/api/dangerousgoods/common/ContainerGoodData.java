package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Also known as IMDG
 */
@Value
@Builder(toBuilder = true)
public class ContainerGoodData implements GoodData {

    @ApiModelProperty(value = "The name of the container good")
    String name;

    @ApiModelProperty(value = "The UN code of the container good")
    @NotNull @NotBlank String unCode;

    @ApiModelProperty(value = "The IMO hazard class of the container good")
    String hazardClass;

    @ApiModelProperty(value = "Any risks in addition to the class to which dangerous goods are assigned; and which is determined by a requirement to have a subsidiary risk.")
    String subsidiaryRisk;

    @ApiModelProperty(value = "The code of the emergency response procedure for vessels carrying dangerous goods in case of fire")
    String emsFireCode;

    @ApiModelProperty(value = "The code of the emergency response procedures for vessels carrying dangerous goods in case of spillage")
    String emsSpillageCode;

    @ApiModelProperty(value = "Extra information about the container good")
    String properties;

    @ApiModelProperty(value = "The code of GDS of the container good")
    String gdsCode;

    @ApiModelProperty(value = "The segregation group of the container good")
    String segregationGroup;

    @ApiModelProperty(value = "Indication of the level of danger")
    PackingGroup packingGroup;

    @ApiModelProperty(value = "Indication if goodData originates from EMSA (European Maritime Safety Agency")
    boolean emsaGoodData;

    @Override
    public String getType() {
        return "containerGood";
    }

    @Override
    public String getStowageType() {
        return "container";
    }
}
