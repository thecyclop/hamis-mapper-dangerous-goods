package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.portbase.bezoekschip.common.api.common.visit.Port;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Tank.class, name = "tank"),
        @JsonSubTypes.Type(value = Hold.class, name = "hold"),
        @JsonSubTypes.Type(value = Container.class, name = "container"),
        @JsonSubTypes.Type(value = BreakBulk.class, name = "breakBulk")})
public interface Stowage {
    @ApiModelProperty(required = true, value = "The unique identifier of the stowage. For tank and hold this is the stowage position. For container this is the container number.")
    @NotNull @NotBlank String getStowageNumber();

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotBlank String getType();

    @ApiModelProperty(value = "The port where the dangerous good is discharged")
    @Valid Port getPortOfDischarge();

    @ApiModelProperty(value = "The port where the dangerous good is loaded")
    @Valid Port getPortOfLoading();

    @NotNull @PositiveOrZero BigDecimal getWeight(); //[10,3]

    Stowage withWeight(BigDecimal weight);

    String getGoodId();

    @JsonIgnore
    default boolean isBulk() {
        switch (getType()) {
            case "tank":
            case "hold":
                return true;
        }
        return false;
    }
}
