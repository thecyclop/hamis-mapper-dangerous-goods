package com.portbase.bezoekschip.common.api.common.visit;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Value
@Builder(toBuilder = true, builderClassName = "Builder")
@ApiModel(description = "The berth that is visited by the vessel")
public class Berth {

    @ApiModelProperty(value = "The name of the berth")
    @NotNull @NotBlank String name;

    @ApiModelProperty(value = "The code of the berth")
    @NotNull @NotBlank String code;
}
