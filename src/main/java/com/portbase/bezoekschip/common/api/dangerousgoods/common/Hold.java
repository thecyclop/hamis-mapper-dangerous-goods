package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import com.portbase.bezoekschip.common.api.common.visit.Port;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import java.math.BigDecimal;

@Value
@Builder(toBuilder = true)
public class Hold implements Stowage {
    String stowageNumber;
    String goodId;
    Port portOfLoading;
    Port portOfDischarge;
    @ApiModelProperty(value = "The gross weight of the solid bulk in the hold (TNE)")
    @Wither
    BigDecimal weight;

    boolean fumigated;
    @Valid Fumigation fumigation;
    
    @AssertTrue(message = "Fumigation info is missing")
    private boolean isFumigationPresent() {
        return !fumigated || fumigation != null;
    }
    
    @Override
    public String getType() {
        return "hold";
    }
}
