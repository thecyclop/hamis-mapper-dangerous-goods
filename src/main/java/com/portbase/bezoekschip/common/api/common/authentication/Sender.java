package com.portbase.bezoekschip.common.api.common.authentication;

import io.fluxcapacitor.common.api.Metadata;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.List;

@Value
@Builder(toBuilder = true)
@AllArgsConstructor
public class Sender {
    @NonNull String userName;
    @NonNull List<String> roles;
    @NonNull String organisationShortName;
}
