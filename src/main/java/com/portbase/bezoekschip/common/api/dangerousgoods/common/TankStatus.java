package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import io.swagger.annotations.ApiModelProperty;

public enum TankStatus {
    
    /*
        Not inert
     */
    
    @ApiModelProperty(value = "A tank with dangerous goods")
    NOT_EMPTY,

    @ApiModelProperty(value = "A tank that has been fully discharged but not cleaned")
    RESIDUE,

    @ApiModelProperty(value = "A tank that has been cleaned but not ventilated")
    EMPTY_NOT_GAS_FREE,

    @ApiModelProperty(value = "A clean, ventilated tank")
    EMPTY,
    
    /*
        Inert
     */
    
    @ApiModelProperty(value = "An inert tank with dangerous goods")
    NOT_EMPTY_INERT,
    
    @ApiModelProperty(value = "An inert tank that has been fully discharged but not cleaned")
    RESIDUE_INERT,

    @ApiModelProperty(value = "An inert tank that has been cleaned but not ventilated")
    EMPTY_INERT_NOT_GAS_FREE,
    
    @ApiModelProperty(value = "A clean, ventilated, inert tank")
    EMPTY_INERT;
    
    public boolean isInert() {
        switch (this) {
            case NOT_EMPTY_INERT:
            case RESIDUE_INERT:
            case EMPTY_INERT_NOT_GAS_FREE:
            case EMPTY_INERT:
                return true;
        }
        return false;
    }
    
    public TankStatus keepInert(Boolean keepInert) {
        if (keepInert == Boolean.FALSE) {
            switch (this) {
                case NOT_EMPTY_INERT: return NOT_EMPTY;
                case RESIDUE_INERT: return RESIDUE;
                case EMPTY_INERT_NOT_GAS_FREE: return EMPTY_NOT_GAS_FREE;
                case EMPTY_INERT: return EMPTY;
            }
        }
        return this;
    }
}
