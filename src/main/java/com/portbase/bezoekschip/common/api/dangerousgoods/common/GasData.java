package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;

/**
 * Also known as IGC
 */
@Value
@Builder
public class GasData implements FluidData {

    @ApiModelProperty(value = "The name of the gas")
    String name;

    @ApiModelProperty (value = "The UN code of the gas")
    String unCode;
    
    @ApiModelProperty(value = "The GDS code of the gas")
    String gdsCode;

    @ApiModelProperty (value = "Indication if goodData originates from EMSA (European Maritime Safety Agency")
    boolean emsaGoodData;

    @Override
    public String getType() {
        return "gas";
    }
}
