package com.portbase.bezoekschip.common.api.visit;

import com.portbase.bezoekschip.common.api.common.visit.PortOfCall;
import com.portbase.bezoekschip.common.api.common.visit.Vessel;
import com.portbase.bezoekschip.common.api.common.visit.VisitDeclaration;
import com.portbase.bezoekschip.common.api.dangerousgoods.common.DangerousGoods;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotNull;

@Value
@Builder
public class ApiVisit {

    @ApiModelProperty(value = "The call reference number of the visit")
    @NotNull String crn;

    @NotNull PortOfCall portOfCall;

    @NotNull Vessel vessel;

    @NotNull VisitDeclaration visitDeclaration;

    DangerousGoods dangerousGoodsDeclaration;

    @ApiModelProperty(value = "Indication whether the port visit is cancelled")
    boolean cancelled;
}
