package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Value;

import javax.validation.constraints.NotNull;

@Value
public class Radionuclide {
    @ApiModelProperty(value = "Class or division of radionuclide")
    @NotNull String name;
}

