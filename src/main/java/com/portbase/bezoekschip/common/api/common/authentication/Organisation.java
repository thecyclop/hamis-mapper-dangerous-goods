package com.portbase.bezoekschip.common.api.common.authentication;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Value
@Builder(toBuilder = true, builderClassName = "Builder")
@ApiModel(value = "Owner: The organisation that is accountable for reporting the port visit \n" +
        "Declarant: The organisation that is responsible for reporting the port visit \n" +
        "CargoDeclarant: The organisation that is responsible for reporting the cargo on board of the vessel ")
public class Organisation {

    @ApiModelProperty(value = "The full name of the organisation")
    @NotNull @NotBlank String fullName;

    @ApiModelProperty(value = "The unique identifier of the organisation, issued by the port authority")
    @NotNull @NotBlank String portAuthorityId;

    @ApiModelProperty(value = "The short name of the organisation")
    @NotNull @NotBlank String shortName;

    @ApiModelProperty(value = "The email address of the organisation")
    String emailAddress;

    @ApiModelProperty(value = "The address of the organisation")
    String address;

    @ApiModelProperty(value = "The city where the organisation is located")
    String city;

    @ApiModelProperty(value = "The UN code of the country where the organisation is located")
    String countryUnCode;

    @ApiModelProperty(value = "The name of the country where the organisation is located")
    String countryName;

    @ApiModelProperty(value = "The zip code of the organisation")
    String zipCode;

    @ApiModelProperty(value = "The phone number of the organisation")
    String phoneNumber;

    @ApiModelProperty(value = "The fax number of the organisation")
    String faxNumber;

    @ApiModelProperty(value = "The contact person of the organisation")
    String contact;

    @ApiModelProperty(value = "The EORI number of the organisation, issued by customs")
    String customsEORINumber;

    @ApiModelProperty(value = "The EAN number of the organisation")
    String ean;
}
