package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Value;
import lombok.experimental.Wither;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.List;

import static com.portbase.bezoekschip.common.api.dangerousgoods.common.TankStatus.EMPTY_INERT;
import static com.portbase.bezoekschip.common.api.dangerousgoods.common.TankStatus.EMPTY_INERT_NOT_GAS_FREE;
import static com.portbase.bezoekschip.common.api.dangerousgoods.common.TankStatus.NOT_EMPTY_INERT;
import static com.portbase.bezoekschip.common.api.dangerousgoods.common.TankStatus.RESIDUE_INERT;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

@Value
public class Inerting implements Handling {

    @ApiModelProperty(value = "The berth where the inerting is performed")
    String berthVisitId;

    @ApiModelProperty(value = "The identifiers of tanks selected for inerting")
    @NotNull @NotEmpty List<@NotBlank String> tankNumbers;

    @Override
    public String getType() {
        return "inerting";
    }

    @Wither
    List<Stowage> stowageBefore;

    @Override
    public List<Stowage> getAction() {
        return stowageBefore.stream()
                .filter(s -> s instanceof Tank && tankNumbers.contains(s.getStowageNumber()))
                .map(s -> {
                    Tank tank = (Tank) s;
                    return tank.toBuilder().tankStatus(computeNewStatus(tank.getTankStatus())).build();
                })
                .collect(toList());
    }

    private static TankStatus computeNewStatus(TankStatus current) {
        switch (current) {
            case NOT_EMPTY: return NOT_EMPTY_INERT;
            case RESIDUE: return RESIDUE_INERT;
            case EMPTY_NOT_GAS_FREE: return EMPTY_INERT_NOT_GAS_FREE;
            case EMPTY: return EMPTY_INERT;
        }
        return current;
    }
    
    @Null(message = "Stowage cannot be inerted: ${validatedValue}")
    private String getHandlingErrors() {
        StringBuilder errorBuilder = new StringBuilder();
        tankNumbers.forEach(s -> {
            Tank existing = (Tank) stowageBefore.stream()
                    .filter(e -> e instanceof Tank && e.getStowageNumber().equals(s))
                    .findFirst().orElse(null);
            if (existing == null) {
                errorBuilder.append(format("tank %s is missing", s));
            }
        });
        String error = errorBuilder.toString();
        return error.isEmpty() ? null : error;
    }
}
