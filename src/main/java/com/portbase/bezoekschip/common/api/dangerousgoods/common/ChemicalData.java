package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;

/**
 * Also known as IBC
 */
@Value
@Builder 
public class ChemicalData implements FluidData {
    @ApiModelProperty (value = "The name of the chemical")
    String name;

    @ApiModelProperty (value = "The pollution category of the chemical")
    String pollutionCategory;

    @ApiModelProperty (value = "The IMO hazard class of the chemical")
    String hazardClass;

    @ApiModelProperty (value = "Additional information regarding the chemical. For example the indication whether viscosity is applicable and if the melting point must be reported in the shipping document")
    String specOpRequirements;

    @ApiModelProperty(value = "The GDS code of the chemical")
    String gdsCode;

    @ApiModelProperty (value = "Indication if goodData originates from EMSA (European Maritime Safety Agency")
    boolean emsaGoodData;

    @Override
    public String getType() {
        return "chemical";
    }
}
