package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Value;

import javax.validation.constraints.AssertTrue;
import java.math.BigDecimal;

@Value
public class Chemical implements Fluid {
    String id;
    BigDecimal flashPoint;
    String remarks;

    @ApiModelProperty(reference = "#/definitions/GoodData")
    ChemicalData goodData;

    @ApiModelProperty (value = "The viscosity of the chemical at 20˚C")
    BigDecimal viscosity;

    @ApiModelProperty (value = "The temperature at which the chemical must be discharged")
    BigDecimal heatingOrder;

    @ApiModelProperty (value = "The melting point of the chemical")
    BigDecimal meltingPoint;

    @ApiModelProperty (value = "The temperature at which the chemical has a viscosity of 50 mPa/s")
    BigDecimal criticalTemperature;

    @Override
    public String getType() {
        return "chemical";
    }

    @AssertTrue(message = "Critical temperature is required if viscosity > 50 mPa/s")
    private boolean hasCriticalTemperature() {
        return criticalTemperature != null || viscosity == null || viscosity.compareTo(new BigDecimal(50)) <= 0;
    }

    @AssertTrue(message = "Critical temperature should only be registered if viscosity > 50 mPa/s")
    private boolean hasIllegalCriticalTemperature() {
        return criticalTemperature == null || (viscosity != null && viscosity.compareTo(new BigDecimal(50)) > 0);
    }
}
