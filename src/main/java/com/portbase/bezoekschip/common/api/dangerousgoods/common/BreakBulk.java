package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import com.portbase.bezoekschip.common.api.common.visit.Port;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Value
@Builder(toBuilder = true)
public class BreakBulk implements Stowage {
    String stowageNumber;
    String goodId;
    Port portOfLoading;
    Port portOfDischarge;
    @ApiModelProperty(value = "The gross weight of the break bulk cargo (kg)")
    @Wither
    BigDecimal weight;

    @NotBlank String position;
    @NotNull @Positive Integer numberOfOuterPackages;
    @Valid PackageType outerPackageType;
    @PositiveOrZero Integer numberOfInnerPackages;
    @Valid PackageType innerPackageType;
    boolean transportInLimitedQuantity;
    @ApiModelProperty(value = "The net weight of the break bulk cargo (kg)")
    BigDecimal netWeight;
    @ApiModelProperty(value = "The net explosive mass of the cargo (kg)")
    BigDecimal netExplosiveMass;

    @AssertTrue(message = "Gross weight should be larger or equal than net weight")
    private boolean isGrossWeightMoreThanNetWeight() {
        return netWeight == null || weight.compareTo(netWeight) >= 0;
    }

    @Override
    public String getType() {
        return "breakBulk";
    }

}
