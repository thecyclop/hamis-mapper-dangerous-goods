package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder(toBuilder = true, builderClassName = "Builder")
@EqualsAndHashCode
public class Radioactivity {

    @ApiModelProperty(value = "Package category of radioactivity")
    PackageCategory packageCategory;

    @ApiModelProperty(value = "Radioactive identification")
    String identification;

    @ApiModelProperty(value = "Radioactivity level in Bq")
    Long level;

    @ApiModelProperty(value = "Amount of radiation at the package")
    TransportIndex transportIndex;

    @ApiModelProperty(value = "Amount of radiation at 1 meter from the package")
    BigDecimal criticalSafetyIndex; // 3 tekens max, inclusief punt

    @ApiModelProperty(value = "License number")
    String licenseNumber;

    @ApiModelProperty(value = "Additional information on the radioactive goods")
    String remarks;


}

