package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;

public interface Fluid extends Good {
    @ApiModelProperty(value = "The temperature in degrees Celsius at which a liquid will give off enough flammable vapour to be ignited")
    @Digits(integer = 3, fraction = 0)
    BigDecimal getFlashPoint();
}
