package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Value;
import lombok.experimental.Wither;

import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

import static com.portbase.bezoekschip.common.api.dangerousgoods.common.TankStatus.EMPTY;
import static com.portbase.bezoekschip.common.api.dangerousgoods.common.TankStatus.EMPTY_INERT_NOT_GAS_FREE;
import static com.portbase.bezoekschip.common.api.dangerousgoods.common.TankStatus.EMPTY_NOT_GAS_FREE;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

@Value
public class Washing implements Handling {

    @ApiModelProperty(value = "The berth where the washing of the tank or hold is performed")
    String berthVisitId;

    @ApiModelProperty(value = "The identifiers of tanks selected for washing")
    @NotNull @NotEmpty List<@NotBlank String> tankNumbers;

    @ApiModelProperty(value = "Indicates whether or not the washing procedure was a medium/commercial wash or not. If true AND otherDestination is true, the washing is assumed to have concluded.")
    @NotNull Boolean mediumOrCommercialWash;

    @ApiModelProperty(value = "Indicates whether or not the contents of the tank have been removed from the tank to another destination after washing. If true AND mediumOrCommercialWash is true, the washing is assumed to have concluded.")
    OtherDestination otherDestination;

    @ApiModelProperty(value = "Indicates whether or not the tank was ventilated after a medium or commercial wash")
    Boolean ventilated;

    @ApiModelProperty(value = "Indicates the tank to which has been pumped over to after a medium or commercial wash")
    String otherCargoTank;

    @ApiModelProperty(value = "The date and time when the washing of the tank is performed at the berth")
    @NotNull Instant timestamp;

    @ApiModelProperty(value = "The remarks about the washing of the tank at the berth")
    String remarks;

    @Override
    public String getType() {
        return "washing";
    }

    @Wither
    List<Stowage> stowageBefore;

    @AssertTrue(message = "Value for other destination is required")
    private boolean hasOtherDestination() {
        return !(mediumOrCommercialWash && otherDestination == null);
    }

    @AssertTrue(message = "Value for ventilation is required")
    private boolean hasVentilated() {
        if (!mediumOrCommercialWash || otherDestination == null || ventilated != null) {
            return true;
        }
        return otherDestination == OtherDestination.SAME_CARGO_TANK;
    }

    @AssertFalse(message = "Value for otherCargoTank is required")
    private boolean hasOtherCargoTank() {
        return otherDestination == OtherDestination.OTHER_CARGO_TANK && otherCargoTank == null;
    }

    @Override
    public List<Stowage> getAction() {
        if (mediumOrCommercialWash && otherDestination != OtherDestination.SAME_CARGO_TANK) {
            return stowageBefore.stream()
                    .filter(s -> s instanceof Tank && tankNumbers.contains(s.getStowageNumber()))
                    .map(s -> {
                        Tank tank = (Tank) s;
                        TankStatus newStatus = ventilated 
                                ? EMPTY : tank.isInert() ? EMPTY_INERT_NOT_GAS_FREE : EMPTY_NOT_GAS_FREE;
                        return tank.toBuilder().tankStatus(newStatus).build();
                    }).collect(toList());
        }
        return stowageBefore.stream()
                .filter(s -> s instanceof Tank && tankNumbers.contains(s.getStowageNumber())).collect(toList());
    }

    @Null(message = "Stowage cannot be washed: ${validatedValue}")
    private String getHandlingErrors() {
        StringBuilder errorBuilder = new StringBuilder();
        tankNumbers.forEach(s -> {
            Tank existing = (Tank) stowageBefore.stream()
                    .filter(e -> e instanceof Tank && e.getStowageNumber().equals(s))
                    .findFirst().orElse(null);
            if (existing == null) {
                errorBuilder.append(format("tank %s is missing", s));
                return;
            }
            if (existing.getWeight().compareTo(BigDecimal.ZERO) > 0) {
                errorBuilder.append(format("tank %s is not empty", s));
            }
            if (existing.isClean()) {
                errorBuilder.append(format("tank %s is already clean", s));
            }
        });
        String error = errorBuilder.toString();
        return error.isEmpty() ? null : error;
    }
}
