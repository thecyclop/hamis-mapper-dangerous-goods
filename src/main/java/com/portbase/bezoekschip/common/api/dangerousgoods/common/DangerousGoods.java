package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.portbase.bezoekschip.common.api.common.authentication.Organisation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Singular;
import lombok.Value;
import lombok.experimental.NonFinal;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import java.beans.ConstructorProperties;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Value
@NonFinal
@ApiModel(description = "The dangerous goods information of the port visit")
@Builder(toBuilder = true, builderClassName = "Builder")
public class DangerousGoods {

    @ApiModelProperty(value = "Goods arriving or loaded on the vessel that visits the port of call")
    @Singular
    @NotNull List<@NotNull @Valid Good> goods;

    @Singular(value = "stowageAtArrival")
    @ApiModelProperty(value = "The stowage of the vessel at arrival at the port of call")
    @NotNull List<@NotNull @Valid Stowage> stowageAtArrival;

    @Singular
    @ApiModelProperty(value = "The handlings that are performed on the vessel during the port visit")
    @NotNull List<@NotNull @Valid Handling> handlings;

    @ApiModelProperty(value = "The organisation (carrier or back-office) that sent in the declaration")
    @NotNull Organisation sender;

    @ConstructorProperties({"goods", "stowageAtArrival", "handlings", "sender"})
    public DangerousGoods(List<Good> goods, List<Stowage> stowageAtArrival, List<Handling> handlings, Organisation sender) {
        this.goods = goods;
        this.sender = sender;
        this.stowageAtArrival = stowageAtArrival;
        AtomicReference<List<Stowage>> stowage = new AtomicReference<>(stowageAtArrival);
        this.handlings = handlings.stream().map(h -> {
            Handling augmentedHandling = h.withStowageBefore(stowage.get());
            stowage.set(augmentedHandling.getStowageAfter());
            return augmentedHandling;
        }).collect(Collectors.toList());
    }

    public static DangerousGoods add(DangerousGoods existing, DangerousGoods other) {
        if (existing == null) {
            return other;
        }
        List<Good> goods = new ArrayList<>(existing.goods);
        List<Stowage> stowageAtArrival = new ArrayList<>(existing.stowageAtArrival);
        List<Handling> handlings = new ArrayList<>(existing.handlings);
        goods.addAll(other.goods);
        stowageAtArrival.addAll(other.stowageAtArrival);
        handlings.addAll(other.handlings);
        return DangerousGoods.builder().goods(goods).stowageAtArrival(stowageAtArrival).handlings(handlings).build();
    }

    @SuppressWarnings("unchecked")
    public <T extends Good> T findGood(String id) {
        return (T) goods.stream().filter(g -> g.getId().equals(id)).findFirst().orElse(null);
    }

    @JsonIgnore
    public List<Stowage> getStowageAtDeparture() {
        return handlings.isEmpty() ? stowageAtArrival : handlings.get(handlings.size() - 1).getStowageAfter();
    }

    @AssertTrue(message = "Radioactivity information is missing")
    private boolean hasRadioactiveMaterial() {
        return Stream.concat(stowageAtArrival.stream(), handlings.stream().filter(h -> h instanceof Loading)
                .flatMap(h -> ((Loading) h).getStowages().stream())).filter(s -> s instanceof Container).allMatch(c -> {
            Container container = (Container) c;
            if (container.getRadioactivity() == null) {
                ContainerGood good = findGood(c.getGoodId());
                return !good.isRadioactive() && !good.getGoodData().getHazardClass().startsWith("7");
            }
            return true;
        });
    }

    @JsonIgnore
    public List<Stowage> getTransitStowage() {
        return stowageAtArrival.stream()
                .flatMap(s -> {
                    BigDecimal transitWeight = getTransitWeight(s);
                    if (transitWeight != null) {
                        if (transitWeight.equals(s.getWeight())
                                || transitWeight.compareTo(BigDecimal.ZERO) > 0) {
                            return Stream.of(s.withWeight(transitWeight));
                        }
                    }
                    return Stream.empty();
                }).collect(toList());
    }
    
    private BigDecimal getTransitWeight(Stowage stowage) {
        if (stowage.isBulk()) {
            if (stowage instanceof Tank) {
                switch (((Tank) stowage).getTankStatus()) {
                    case EMPTY_INERT:
                    case EMPTY:
                        return null;
                }
                if (stowage.getWeight().compareTo(BigDecimal.ZERO) <= 0) {
                    return getDepartingWeight(stowage);
                }
            }
            for (Handling handling : handlings) {
                if (handling instanceof Discharge) {
                    for (DischargedStowage action : ((Discharge) handling).getStowages()) {
                        if (Objects.equals(stowage.getType(), action.getType())
                                && Objects.equals(stowage.getStowageNumber(), action.getStowageNumber())) {
                            if (action.getEmptied()) {
                                return null;
                            }
                            stowage = stowage.withWeight(stowage.getWeight().subtract(action.getAmount()));
                        }
                    }
                }
            }
            return stowage.getWeight().compareTo(BigDecimal.ZERO) <= 0 ? null : stowage.getWeight();
        } else {
            return getDepartingWeight(stowage);
        }
    }

    private BigDecimal getDepartingWeight(Stowage stowage) {
        for (Stowage after : getStowageAtDeparture()) {
            if (stowage.equals(after)) {
                return stowage.getWeight();
            }
        }
        return null;
    }
}
