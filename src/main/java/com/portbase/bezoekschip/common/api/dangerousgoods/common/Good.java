package com.portbase.bezoekschip.common.api.dangerousgoods.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = ContainerGood.class, name = "containerGood"),
        @JsonSubTypes.Type(value = Solid.class, name = "solid"),
        @JsonSubTypes.Type(value = Oil.class, name = "oil"),
        @JsonSubTypes.Type(value = Gas.class, name = "gas"),
        @JsonSubTypes.Type(value = Chemical.class, name = "chemical")})
public interface Good {
    @NotNull @Valid GoodData getGoodData();
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotBlank String getType();
    @NotBlank String getId();

    @ApiModelProperty(value = "Any remarks regarding the good")
    String getRemarks();
}
