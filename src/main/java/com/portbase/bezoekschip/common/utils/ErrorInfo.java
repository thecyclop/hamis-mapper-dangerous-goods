package com.portbase.bezoekschip.common.utils;

import lombok.Value;

import java.time.Duration;
import java.time.Instant;

@Value
public class ErrorInfo {
    Instant timeOfFirstFail;
    boolean alertIsSent;

    public boolean shouldSendAlert(Duration gracePeriod) {
        return !alertIsSent && timeOfFirstFail.isBefore(Instant.now().minus(gracePeriod));
    }
}