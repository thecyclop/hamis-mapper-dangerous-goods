package com.portbase.bezoekschip.common.utils;

import lombok.Value;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Value
public class DateRange {
    @NotNull LocalDate start, end;
}
