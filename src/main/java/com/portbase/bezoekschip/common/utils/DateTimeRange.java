package com.portbase.bezoekschip.common.utils;

import lombok.Value;

import javax.validation.constraints.NotNull;
import java.time.Instant;

@Value
public class DateTimeRange {
    @NotNull Instant start, end;
}
